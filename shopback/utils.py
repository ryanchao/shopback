import unicodedata
from types import FunctionType, ModuleType
from typing import Any


def qualname(obj: Any, level: int = -1) -> str:

    if isinstance(obj, FunctionType) or isinstance(obj, type):
        name = f"{obj.__module__}.{obj.__qualname__}"

    elif isinstance(obj, ModuleType):
        name = f"{obj.__name__}"

    else:
        name = ""

    return ".".join(name.split(".")[-level:]) if level > 0 else name


def split_and_join(text: str, splitter: str = "", joiner: str = "") -> str:
    """Split string first, then join back to a new string by specified delimiter."""

    return joiner.join(text.split(splitter or None))


def halfwidth(text: str, form="NFKD") -> str:
    """Convert the string to halfwidth"""

    return unicodedata.normalize(form, text)
