# SHOPBACK ASSIGNNENT


## 3party Libraries

* click: commandline build tool, [doc](https://click.palletsprojects.com/en/7.x/)
* jieba: tokenizer for Mandarin corpus, [repo](https://github.com/fxsjy/jieba)
* regex: support more functionality than built-in `re` module, [pypi](https://pypi.org/project/regex/)
* numpy: numeric computing tool, [doc](https://numpy.org/)
* pandas: data analytics tool for tabular data, [doc](https://pandas.pydata.org/pandas-docs/stable/index.html)
* sklearn: for tfidf computation, [doc](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html)

# Setting Enviroment for Both Questions

* Install `poetry` in host enviroment for python dependency management
	* `$ pip install poetry`
  
* Create virtual enviroment in this folder
	* `$ python -m venv .venv`

* Install the dependencies defined in `pyproject.toml` by `poetry`, all packages will be installed under `.venv/lib/python3.6/site-packages/`
	* `$ poetry install`
  
  
* Project layout is something like this:

```
.
├── .git
├── .venv
├── README.md
├── poetry.lock
├── pyproject.toml
├── q1
│   ├── description.txt
│   ├── news.rss
│   ├── output.txt
│   └── readme.txt
├── q2
│   ├── output.txt
│   ├── readme.txt
│   └── tfidf.vocabs
├── shopback
│   ├── __init__.py
│   ├── cli
│   │   ├── __init__.py
│   │   └── main.py
│   ├── main.py
│   ├── string.py
│   └── utils.py
```

* Source codes is in `./shopback` folder

## Reproduce Outputs ##

### Q1 ###

* `description.txt`
    * `$ poetry run shopback extract -xml q1/news.rss > q1/description.txt`
    * the implementation: `shopback/cli/main.py::extract`

* `output.txt`
    * `$ poetry run shopback tokenize -in q1/description.txt > q1/output.txt`
    * the implementation: `shopback/cli/main.py::tokenize`

### Q2 ###

* `output.txt`
    * `$ poetry run shopback tfidf -tok q1/output.txt > q2/output.txt`
    * the implementation: `shopback/cli/main.py::tfidf`
